# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is demo project for showcasing working example of Spring boot application and mySql integration for assigned problem statement
* 0.0.1-SNAPSHOT

### How do I get set up? ###

* Clone the repository link (git clone https://asifBH@bitbucket.org/asifBH/demo-application.git)
* Install maven (http://maven.apache.org/install.html)
* Install lombok (https://projectlombok.org/setup/overview)
* Here we have used mysql db version mysql-5.6.48. we need to install given version and create schema with name "demo" All DB tables would be created once code is run
* If we are using eclipse/intellij IDE we need to open the above clone repository and run following command
	* Open pom.xml and run mvn clean install
	* Update maven project
	* Go to class "com.demo.DemoApplication" and run it as Java application

### Insert below data in DB  ###


INSERT INTO user (ID, USER_COUNTRY, USER_NAME, CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("1","US","USER US","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO user (ID, USER_COUNTRY, USER_NAME, CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("2","IND","USER INDIA","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");


INSERT INTO offer (ID, OFFER_NAME, OFFER_DESCRIPTION, CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("2","FREE","FREE OFFER","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO offer (ID, OFFER_NAME, OFFER_DESCRIPTION, CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("1","ONE TIME","ONE TIME PAYMENT","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO offer (ID, OFFER_NAME, OFFER_DESCRIPTION, CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("3","12SUB","12 month subscripton","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO offer (ID, OFFER_NAME, OFFER_DESCRIPTION, CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("4","6SUB","6 month subscripton","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");


INSERT INTO course (ID, COURSE_NAME, COURSE_DESCRIPTION, BASE_PRICE , OFFER_ID,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("1","Docker tutorial","This course will help you docker in depty",0,"2","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO course (ID, COURSE_NAME, COURSE_DESCRIPTION, BASE_PRICE , OFFER_ID,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("2","Kubernates tutorial","This course will help you kubernates in depty",100,"1","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO course (ID, COURSE_NAME, COURSE_DESCRIPTION, BASE_PRICE , OFFER_ID,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("3","AWS tutorial","This course will help you AWS in depty",150,"3","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");


INSERT INTO pricing_components_master (ID, PRICING_COMPONENT_NAME,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("1","Sales Tax","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO pricing_components_master (ID, PRICING_COMPONENT_NAME,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("2","Convertion Tax","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO pricing_components_master (ID, PRICING_COMPONENT_NAME,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("3","Shipping Tax","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");

INSERT INTO pricing_component (ID, PRICING_COMPONENT_MASTER_ID,COUNTRY_CODE,PERCENTAGE,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("1","1","US","10","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO pricing_component (ID, PRICING_COMPONENT_MASTER_ID,COUNTRY_CODE,PERCENTAGE,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("2","1","IND","20","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO pricing_component (ID, PRICING_COMPONENT_MASTER_ID,COUNTRY_CODE,PERCENTAGE,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("3","2","US","10","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");
INSERT INTO pricing_component (ID, PRICING_COMPONENT_MASTER_ID,COUNTRY_CODE,PERCENTAGE,CREATED_ON, UPDATED_ON, CREATED_BY, UPDATED_BY)
VALUES ("4","3","IND","5","2020-04-17 16:17:58.463", "2020-04-17 16:17:58.463", "API", "API");


### Curls for some of use cases ###

* User not found for given user id
$ curl -i -H 'Content-Type: application/json' -H 'userId: 10' -X GET "http://localhost:8081/demo-service/api/course/info/1"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   102    0   102    0     0    842      0 --:--:-- --:--:-- --:--:--   850HTTP/1.1 404
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 06 May 2020 21:41:32 GMT

{"error_code":"DA404","error_message":"User not found","error_detail":"User not found for user id 10"}

* Course not found
$ curl -i -H 'Content-Type: application/json' -H 'userId: 1' -X GET "http://localhost:8081/demo-service/api/course/info/10"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   108    0   108    0     0   1963      0 --:--:-- --:--:-- --:--:--  1963HTTP/1.1 404
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 06 May 2020 21:42:04 GMT

{"error_code":"DA404","error_message":"Course not found","error_detail":"Course not found for course id 10"}

* User search for course which is free

curl -i -H 'Content-Type: application/json' -H 'userId: 1' -X GET "http://localhost:8081/demo-service/api/course/info/1"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   191    0   191    0     0   6821      0 --:--:-- --:--:-- --:--:--  7074HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 06 May 2020 21:44:20 GMT

{"course_id":"1","course_name":"Docker tutorial","course_description":"This course will help you docker in depty","offer_name":"FREE","base_price":0.0,"price_break_down":[],"total_price":0.0}

* User from india search for course user id :- 2 course id :- 2

$ curl -i -H 'Content-Type: application/json' -H 'userId: 2' -X GET "http://localhost:8081/demo-service/api/course/info/2"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   347    0   347    0     0    882      0 --:--:-- --:--:-- --:--:--   885HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 06 May 2020 21:45:35 GMT

{"course_id":"2","course_name":"Kubernates tutorial","course_description":"This course will help you kubernates in depty","offer_name":"ONE TIME","base_price":100.0,"price_break_down":[{"component_name":"Sales Tax","rate":20.0,"price_on_base_price":20.0},{"component_name":"Shipping Tax","rate":5.0,"price_on_base_price":5.0}],"total_price":125.0}

* User from USA search for same course user id :- 1 course id :- 2 (He would get different tax components and price)

$ curl -i -H 'Content-Type: application/json' -H 'userId: 1' -X GET "http://localhost:8081/demo-service/api/course/info/2"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   351    0   351    0     0   6267      0 --:--:-- --:--:-- --:--:--  6381HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 06 May 2020 21:46:05 GMT

{"course_id":"2","course_name":"Kubernates tutorial","course_description":"This course will help you kubernates in depty","offer_name":"ONE TIME","base_price":100.0,"price_break_down":[{"component_name":"Sales Tax","rate":10.0,"price_on_base_price":10.0},{"component_name":"Convertion Tax","rate":10.0,"price_on_base_price":10.0}],"total_price":120.0}

