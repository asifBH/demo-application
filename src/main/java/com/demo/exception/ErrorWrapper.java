package com.demo.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ErrorWrapper {

	@JsonProperty("error_code")
	private String errorCode;
	@JsonProperty("error_message")
	private String errorMessage ;
	@JsonProperty("error_detail")
	private String errorDetail;
	
	/**
	 * Error code
	 * 
	 * @return errorCode
	 **/
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public ErrorWrapper errorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	/**
	 * Error message
	 * 
	 * @return errorMessage
	 **/
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ErrorWrapper errorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		return this;
	}

	/**
	 * Error detail
	 * 
	 * @return errorDetail
	 **/
	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	public ErrorWrapper errorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
		return this;
	}
}
