package com.demo.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DemoException extends RuntimeException {


	private static final long serialVersionUID = 1L;

	private final HttpStatus status;
	
	private final ErrorWrapper errorWrapper;

	public DemoException(HttpStatus status, ErrorWrapper errorWrapper) {
		super();
		this.status = status;
		this.errorWrapper = errorWrapper;
	}
}
