package com.demo.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.apachecommons.CommonsLog;

@ControllerAdvice(basePackages = "com.demo")
@CommonsLog
public class ErrorHandlingController {

	/**
	 * Central exception handler and generate common custom response
	 *
	 * @param request
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(Throwable.class)
	@ResponseBody
	public ResponseEntity<?> handleControllerException(final HttpServletRequest request, final Throwable exception) {
		HttpStatus status = null;
		ErrorWrapper errorWrapper = null;
		if (exception instanceof DemoException) {
			status = ((DemoException) exception).getStatus();
			errorWrapper = ((DemoException) exception).getErrorWrapper();
		}
		else {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			errorWrapper = new ErrorWrapper("DA500", "Internal Server Error", "Internal Server Error");
		}
		StringBuffer requestedURL = request.getRequestURL();
		log.info("Requested URL: "+ requestedURL);
		log.error("exception : "+ exception);
		return new ResponseEntity<>(errorWrapper, status);
	}

}
