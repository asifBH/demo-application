package com.demo.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.demo.dao.CourseDao;
import com.demo.dao.PricingComponentDao;
import com.demo.dao.UserDao;
import com.demo.dto.CourseInfoDto;
import com.demo.dto.PricingComponentsDto;
import com.demo.exception.DemoException;
import com.demo.exception.ErrorWrapper;
import com.demo.model.Course;
import com.demo.model.Offer;
import com.demo.model.PricingComponent;
import com.demo.model.User;
import com.demo.utility.DTOUtills;
import com.demo.utility.PercentageCalculator;

import lombok.extern.apachecommons.CommonsLog;

@Service
@CommonsLog
public class CourseProcessor {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private CourseDao courseDao;
	
	@Autowired
	private PricingComponentDao pricingComponentDao;

	public CourseInfoDto getCourseInfo(String userId , String courseId) throws Exception {
		CourseInfoDto courseInfoDto = null;
		List<PricingComponentsDto> listOfPricingComponentDto = new ArrayList<PricingComponentsDto>();
		String offerName = null;
		Optional<User> exsistingUser = userDao.getUserByUserId(userId);
		
		if (!exsistingUser.isPresent()) {
			log.error("User not found for user id " + userId);
			throw new DemoException(HttpStatus.NOT_FOUND, new ErrorWrapper("DA404", "User not found", "User not found for user id " + userId));
		}
		 
		User user = exsistingUser.get();
		Optional<Course> exsistingCourse  = courseDao.getCourseByCourseId(courseId);
		if (!exsistingCourse.isPresent()) {
			log.error("Course not found for course id " + courseId);
			throw new DemoException(HttpStatus.NOT_FOUND, new ErrorWrapper("DA404", "Course not found", "Course not found for course id " + courseId));
		}
		
		Course course = exsistingCourse.get();
		Offer offer = course.getOffer();
		
		if (!ObjectUtils.isEmpty(offer)) {
			offerName = offer.getOfferName();
		}
		if (!offer.getOfferName().equalsIgnoreCase("free")) {
			List<PricingComponent> listOfPricingComponent = pricingComponentDao.getPricingComponentsByCountryCode(user.getUserCountry());
			listOfPricingComponent.stream().forEach(priceComponent -> {
				PricingComponentsDto pricingComponentsDto = new PricingComponentsDto ();
				double amount = PercentageCalculator.calculatePercentage(course.getBasePrice(), priceComponent.getPercentage());
				pricingComponentsDto.setPriceComponentName(priceComponent.getPricingComponentMaster().getPricingComponentName());
				pricingComponentsDto.setPriceComponentRate(priceComponent.getPercentage());
				pricingComponentsDto.setPriceOnBasePrice(amount);
				listOfPricingComponentDto.add(pricingComponentsDto);
			});
		} 
		double total = course.getBasePrice() + listOfPricingComponentDto.stream().map(x -> x.getPriceOnBasePrice()).collect(Collectors.summingDouble(Double::doubleValue));
		courseInfoDto = DTOUtills.populateCourceInfoDto(course, offerName, listOfPricingComponentDto);
		courseInfoDto.setTotalPrice(total);
		return courseInfoDto;
	}
	
}
