package com.demo.dao;

import java.util.List;

import com.demo.model.PricingComponent;

public interface PricingComponentDao {

	public List<PricingComponent> getPricingComponentsByCountryCode (String countryCode) ;
}
