package com.demo.dao;

import java.util.Optional;

import com.demo.model.Course;

public interface CourseDao {

	public Optional<Course> getCourseByCourseId (String courseId);
	
}
