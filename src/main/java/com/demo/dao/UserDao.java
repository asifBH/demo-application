package com.demo.dao;

import java.util.Optional;

import com.demo.model.User;

public interface UserDao {

	public Optional<User> getUserByUserId (String userId);
	
}
