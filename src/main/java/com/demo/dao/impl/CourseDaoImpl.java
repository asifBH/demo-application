package com.demo.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.dao.CourseDao;
import com.demo.model.Course;
import com.demo.repository.CourseRepository;

@Component
public class CourseDaoImpl implements CourseDao {
	
	@Autowired
	private CourseRepository courseRepository;

	@Override
	public Optional<Course> getCourseByCourseId(String courseId) {
		return courseRepository.findById(courseId);
	}

}
