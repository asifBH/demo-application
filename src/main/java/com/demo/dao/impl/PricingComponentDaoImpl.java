package com.demo.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.dao.PricingComponentDao;
import com.demo.model.PricingComponent;
import com.demo.repository.PricingComponentRepository;

@Component
public class PricingComponentDaoImpl implements PricingComponentDao {

	@Autowired
	private PricingComponentRepository pricingComponentRepository;
	
	@Override
	public List<PricingComponent> getPricingComponentsByCountryCode(String countryCode) {
		return pricingComponentRepository.findByCountryCode(countryCode);
	}

}
