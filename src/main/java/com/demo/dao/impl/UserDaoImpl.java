package com.demo.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.dao.UserDao;
import com.demo.model.User;
import com.demo.repository.UserRepository;

@Component
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public Optional<User> getUserByUserId(String userId) {
		return userRepository.findById(userId);
	}

}
