package com.demo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "PRICING_COMPONENTS_MASTER",  uniqueConstraints = @UniqueConstraint(columnNames = { "PRICING_COMPONENT_NAME"} , name = "UK_PRICING_COMPONENT_NAME"))
@Data
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PricingComponentMaster implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3791109209472615362L;
	
	@Id
	@GenericGenerator(name = "GUID" , strategy = "org.hibernate.id.GUIDGenerator")
	@GeneratedValue(generator = "GUID")
	@Column(name = "ID", unique = true, nullable = false)
	private String id;
	
	@Column(name = "PRICING_COMPONENT_NAME", length = 255 )
	private String pricingComponentName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", length = 19)
	private Date createdOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", length = 19)
	private Date updatedOn;
	
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;
	
	@Column(name = "UPDATED_BY", length = 50)
	private String updatedBy;
	
	@OneToMany( mappedBy = "pricingComponentMaster" , cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PricingComponent> pricingComponents = new ArrayList<>();

	public PricingComponentMaster(String id, String pricingComponentName, Date createdOn, Date updatedOn,
			String createdBy, String updatedBy) {
		super();
		this.id = id;
		this.pricingComponentName = pricingComponentName;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}
}
