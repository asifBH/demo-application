package com.demo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "OFFER",  uniqueConstraints = @UniqueConstraint(columnNames = { "OFFER_NAME"} , name = "UK_OFFER_OFFER_NAME"))
@Data
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Offer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7253253646819252057L;
	
	@Id
	@GenericGenerator(name = "GUID" , strategy = "org.hibernate.id.GUIDGenerator")
	@GeneratedValue(generator = "GUID")
	@Column(name = "ID", unique = true, nullable = false)
	private String id;
	
	@Column(name = "OFFER_NAME", length = 100 )
	private String offerName;

	@Column(name = "OFFER_DESCRIPTION", length = 255 )
	private String offerDescription;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", length = 19)
	private Date createdOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", length = 19)
	private Date updatedOn;
	
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;
	
	@Column(name = "UPDATED_BY", length = 50)
	private String updatedBy; 
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "offer")
	private Course course;

	public Offer(String offerName, String offerDescription, Date createdOn, Date updatedOn, String createdBy,
			String updatedBy) {
		super();
		this.offerName = offerName;
		this.offerDescription = offerDescription;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}
}
