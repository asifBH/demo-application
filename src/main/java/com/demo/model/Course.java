package com.demo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "COURSE",  uniqueConstraints = @UniqueConstraint(columnNames = { "COURSE_NAME"} , name = "UK_COURSE_COURSE_NAME"))
@Data
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Course implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1606562526260853852L;
	
	@Id
	@GenericGenerator(name = "GUID" , strategy = "org.hibernate.id.GUIDGenerator")
	@GeneratedValue(generator = "GUID")
	@Column(name = "ID", unique = true, nullable = false)
	private String id;
	
	@Column(name = "COURSE_NAME", length = 100 )
	private String courseName;

	@Column(name = "COURSE_DESCRIPTION", length = 255 )
	private String courseDescription;
	
	@Column(name = "BASE_PRICE")
	private double basePrice;
	
	@Column(name = "OFFER_ID", length = 100 )
	private String offerId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", length = 19)
	private Date createdOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", length = 19)
	private Date updatedOn;
	
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;
	
	@Column(name = "UPDATED_BY", length = 50)
	private String updatedBy;
 
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn( name = "OFFER_ID",insertable = false, updatable = false, nullable = false )
	private Offer offer;

	public Course(String courseName, String courseDescription, Integer basePrice, String offerId, Date createdOn,
			Date updatedOn, String createdBy, String updatedBy) {
		super();
		this.courseName = courseName;
		this.courseDescription = courseDescription;
		this.basePrice = basePrice;
		this.offerId = offerId;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}
}
