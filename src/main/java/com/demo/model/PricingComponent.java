package com.demo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "PRICING_COMPONENT",  uniqueConstraints = @UniqueConstraint(columnNames = { "PRICING_COMPONENT_MASTER_ID", "COUNTRY_CODE"} , name = "UK_PRICING_COMPONENT_MASTER_ID_COUNTRY_CODE"),
      indexes = { @Index (name = "IDX_COUNTRY_CODE", columnList="COUNTRY_CODE", unique = false)})
@Data
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class PricingComponent implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5794434703805600732L;

	@Id
	@GenericGenerator(name = "GUID" , strategy = "org.hibernate.id.GUIDGenerator")
	@GeneratedValue(generator = "GUID")
	@Column(name = "ID", unique = true, nullable = false)
	private String id;
	
	@Column(name = "PRICING_COMPONENT_MASTER_ID", length = 255 )
	private String pricingComponentMasterId;

	@Column(name = "COUNTRY_CODE", length = 100 )
	private String countryCode;

	@Column(name = "PERCENTAGE", length = 100 )
	private double percentage;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", length = 19)
	private Date createdOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", length = 19)
	private Date updatedOn;
	
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;
	
	@Column(name = "UPDATED_BY", length = 50)
	private String updatedBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn( name = "PRICING_COMPONENT_MASTER_ID", insertable = false, updatable = false )
	private PricingComponentMaster pricingComponentMaster;

}
