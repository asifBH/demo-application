package com.demo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "USER",  uniqueConstraints = @UniqueConstraint(columnNames = { "USER_NAME"} , name = "UK_USER_USER_NAME"))
@Data
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -981278959517461906L;
	
	@Id
	@GenericGenerator(name = "GUID" , strategy = "org.hibernate.id.GUIDGenerator")
	@GeneratedValue(generator = "GUID")
	@Column(name = "ID", unique = true, nullable = false)
	private String id;
	
	@Column(name = "USER_NAME", length = 100 )
	private String userName;

	@Column(name = "USER_COUNTRY", length = 255 )
	private String userCountry;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", length = 19)
	private Date createdOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", length = 19)
	private Date updatedOn;
	
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;
	
	@Column(name = "UPDATED_BY", length = 50)
	private String updatedBy;

	public User(String userName, String userCountry, Date createdOn, Date updatedOn, String createdBy,
			String updatedBy) {
		super();
		this.userName = userName;
		this.userCountry = userCountry;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	} 
}
