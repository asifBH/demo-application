package com.demo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CourseInfoDto {

	@JsonProperty("course_id")
	private String courseId;
	
	@JsonProperty("course_name")
	private String courseName;
	
	@JsonProperty("course_description")
	private String courseDescription;
	
	@JsonProperty("offer_name")
	private String offerName;
	
	@JsonProperty("base_price")
	private double basePrice;
	
	@JsonProperty("price_break_down")
	private List<PricingComponentsDto> listOfPricingComponentDto = new ArrayList<PricingComponentsDto>();
	
	@JsonProperty("total_price")
	private double totalPrice;
}
