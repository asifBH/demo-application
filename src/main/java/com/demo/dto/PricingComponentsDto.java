package com.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PricingComponentsDto {

	@JsonProperty("component_name")
	private String priceComponentName;
	
	@JsonProperty("rate")
	private Double priceComponentRate;
	
	@JsonProperty("price_on_base_price")
	private Double priceOnBasePrice;
}
