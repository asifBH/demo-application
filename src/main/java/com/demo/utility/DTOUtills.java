package com.demo.utility;

import java.util.List;

import com.demo.dto.CourseInfoDto;
import com.demo.dto.PricingComponentsDto;
import com.demo.model.Course;

public class DTOUtills {

	public static CourseInfoDto populateCourceInfoDto (final Course course,String offerName,List<PricingComponentsDto> listOfPricingComponentDto ) {
		CourseInfoDto courseInfoDto = new CourseInfoDto();
		courseInfoDto.setListOfPricingComponentDto(listOfPricingComponentDto);
		courseInfoDto.setBasePrice(course.getBasePrice());
		courseInfoDto.setCourseDescription(course.getCourseDescription());
		courseInfoDto.setCourseName(course.getCourseName());
		courseInfoDto.setOfferName(offerName);
		courseInfoDto.setCourseId(course.getId());
		return courseInfoDto;
	}
	
}
