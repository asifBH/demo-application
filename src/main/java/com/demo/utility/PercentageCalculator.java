package com.demo.utility;

public class PercentageCalculator {

	public static Double calculatePercentage (Double basePrice, Double percentage) {
		return basePrice * percentage / 100;
	}
	
}
