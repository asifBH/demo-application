package com.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.model.PricingComponent;

@Repository
public interface PricingComponentRepository extends JpaRepository<PricingComponent, String> {

	public List<PricingComponent> findByCountryCode (String countryCode) ;
}
