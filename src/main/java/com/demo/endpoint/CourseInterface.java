package com.demo.endpoint;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.demo.dto.CourseInfoDto;

@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public interface CourseInterface {

	@GetMapping("/course/info/{courseId}")
	public CourseInfoDto getCourseInfo(@RequestHeader("userId") final String userId,@PathVariable final String courseId) throws Exception;
}
