package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.CourseInfoDto;
import com.demo.endpoint.CourseInterface;
import com.demo.processor.CourseProcessor;

import lombok.extern.apachecommons.CommonsLog;

@RestController
@CommonsLog
public class CourseController implements CourseInterface {

	@Autowired
	private CourseProcessor courseProcessor;
	
	@Override
	public CourseInfoDto getCourseInfo(String userId, String courseId) throws Exception {
		log.debug("user id "+userId+" have requested course info for course id "+courseId );
		return courseProcessor.getCourseInfo(userId, courseId);
		
	}

}
